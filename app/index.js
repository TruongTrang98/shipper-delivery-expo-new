import React from 'react'
import { Router, Scene, Actions } from 'react-native-router-flux'
import { ApolloProvider } from 'react-apollo'
import { Icon } from 'react-native-elements'
import { Provider, inject, observer } from 'mobx-react'
import { AsyncStorage } from 'react-native'

import Login from '../screens/Login'
import OrderDetail from '../screens/orderDetail'
import { Store, Client } from '../tools/index'
import DonHangMoi from '../screens/DonHangMoi'
import DonHangDangNhan from '../screens/DonHangDangNhan'
import TaiKhoan from '../screens/TaiKhoan'
import LichSu from '../screens/LichSu'
import ChangePassword from '../screens/changePassword'

@inject(store => store.store)
@observer
class App extends React.Component {
  componentDidMount = async () => {
    const res = await AsyncStorage.getItem('TOKEN')
    if (res) {
      this.props.Authen.setLogin(true)
      Actions.newOrderTab()
    } else {
      this.props.Authen.setLogin(false)
      Actions.login()
    }
  }

  render () {
    const { Authen: { isLogin } } = this.props
    return (
      <Router>
        <Scene key='root' hideNavBar>
          {isLogin ? (
            <>
              <Scene key='tabBar' tabs tabBarStyle={{ color: '#fff' }} hideNavBar>
                <Scene key='newOrderTab' icon={() => <Icon name='list' />} title='Đơn hàng mới' tabBarOnPress={() => Actions.newOrder()} initial={isLogin} type='reset'>
                  <Scene 
                    initial
                    key='newOrder'
                    component={DonHangMoi}
                    title='Danh sách đơn hàng mới'
                    // onEnter={this.onEnterNewOrderView}
                  />
                </Scene>
                <Scene key='orderReceivedTab' icon={() => <Icon name='check' />} title='Đang nhận' tabBarOnPress={() => Actions.orderReceived()}>
                  <Scene 
                    initial
                    key='orderReceived'
                    component={DonHangDangNhan}
                    title='Đơn hàng đang nhận'
                  />
                </Scene>
                <Scene key='historyTab' icon={() => <Icon name='book' />} title='Lịch sử' tabBarOnPress={() => Actions.history()}>
                  <Scene 
                    initial
                    key='history'
                    component={LichSu}
                    title='Lịch sử'
                  />
                </Scene>
                <Scene key='infoTab' icon={() => <Icon name='person' />} title='Cá nhân' tabBarOnPress={() => Actions.info()}>
                  <Scene 
                    initial
                    key='info'
                    component={TaiKhoan}
                    title='Tài khoản'
                  />
                </Scene>
              </Scene>
              <Scene
                key="orderDetail"
                component={OrderDetail}
                title='Chi tiết đơn hàng'
                back
              />
              <Scene
                key="changePassword"
                component={ChangePassword}
                title='Đổi mật khẩu'
                back
              />
            </>
          ) : (
            <Scene
            key="login"
            component={Login}
            hideNavBar
          />
          )}
        </Scene>
      </Router>
    )
  }
}

const store = new Store()

const DeliveryApp = () => {
  return (
    <Provider store={store}>
      <ApolloProvider client={Client}>
        <App />
      </ApolloProvider>
    </Provider>
  )
}

export default DeliveryApp