import { observable, action } from 'mobx'
import { AsyncStorage } from 'react-native'

class Authen {
  @observable isLogin = false

  @action  
  onLogin = async (token) => {
    this.isLogin = true
    await AsyncStorage.setItem('TOKEN', token)
  }

  @action 
  onLogout = async () => {
    this.isLogin = false
    await AsyncStorage.removeItem('TOKEN')
    await AsyncStorage.clear()
  }

  @action setLogin(value) {
    this.isLogin = value
  }
}

export { Authen }
