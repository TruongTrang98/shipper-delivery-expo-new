import { ApolloClient } from 'apollo-client'
import { Platform } from 'react-native'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink, split } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import { getMainDefinition } from 'apollo-utilities'
import { AsyncStorage } from 'react-native'
import { setContext } from 'apollo-link-context';
import { graphqlUrl, socketEndpoint } from '../../config/index'

// const httpLink = new HttpLink({
//   uri: 'https://graphqldelivery.herokuapp.com/graphqldelivery'
// })

const httpLink = new HttpLink({
  uri: 'http://192.168.1.139:3000/graphqldelivery'
})

// const wsClient = new SubscriptionClient('ws://graphqldelivery.herokuapp.com/graphqldelivery', { reconnect: true })
const wsClient = new SubscriptionClient('ws://192.168.1.139:3000/graphqldelivery', { reconnect: true })

const wsLink = new WebSocketLink({
  // uri: 'ws://graphqldelivery.herokuapp.com/graphqldelivery',
  uri: 'ws://192.168.1.139:3000/graphqldelivery',
  options: {
    reconnect: true,
    connectionParams: async () => ({
      token: await AsyncStorage.getItem('TOKEN') || ''
    })
  }
})

//const wsLink = new WebSocketLink(wsClient)

const authLink = setContext(async (req, { headers }) => {
  const token = await AsyncStorage.getItem('TOKEN');
  console.log(token)
  return {
    ...headers,
    headers: {
      token: token ? `${token}` : '',
    },
  };
});


const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  }, wsLink, httpLink
)

const Client = new ApolloClient({
  link: authLink.concat(link),
  cache: new InMemoryCache(),
  defaultOptions: {
    mutate: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all'
    },
    query: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all'
    },
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'ignore'
    }
  }
})

export { Client }
