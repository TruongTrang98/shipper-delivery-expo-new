/*eslint-disable*/
import React from 'react'
import Spinner from 'react-native-loading-spinner-overlay'
import { StyleSheet } from 'react-native'
import { withApollo as withApo, graphql } from 'react-apollo'
import compose from 'lodash/flowRight'

// data = [
//   { query: ``, variables: {} },
//   { query: ``, options: (props) => ({}) },
//   { mutation: ``, name: '', options: {} }
// ]

const HOCQueryMutation = data => Component => {
  if (data === undefined) return withApo(Component)
  else {
    try {
      let k = -1
      const GraphQLComponent = data.map((QueryOrMutate, idx) => {
        if (QueryOrMutate.query) {
          if (!!QueryOrMutate.variables) {
            return graphql(QueryOrMutate.query, {
              variables: QueryOrMutate.variables,
              options: { fetchPolicy: 'network-only' }
            })
          } else if (!!QueryOrMutate.options) {
            return graphql(QueryOrMutate.query, { options: QueryOrMutate.options, fetchPolicy: 'network-only' })
          }
          return graphql(QueryOrMutate.query)
        } else if(QueryOrMutate.mutation) {
          if (k === -1) k = idx
          return graphql(QueryOrMutate.mutation, { name: QueryOrMutate.name, options: QueryOrMutate.options })
        } else {
          return graphql(QueryOrMutate.subcription, { name: QueryOrMutate.name, options: QueryOrMutate.options })
        }
      })
      const WrapComponent = props => {
        if (!!props.data) {
          const { loading, error } = props.data
          if (error) {
            console.log('HOCQueryMutation error ', error)
            return (
              <Spinner
                visible={true}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
              />
            )
          }
          if (loading) return (
            <Spinner
              visible={true}
              textContent={'Loading...'}
              textStyle={styles.spinnerTextStyle}
            />
          )
        }
        let wrapProps = { ...props }
        // eslint-disable-next-line no-unused-vars
        for (let QueryOrMutate of data) {
          if (QueryOrMutate.mutation) {
            wrapProps.mutate = { ...wrapProps.mutate, [QueryOrMutate.name]: props[QueryOrMutate.name] }
            delete wrapProps[QueryOrMutate.name]
          }
        }
        return <Component {...wrapProps} />
      }
      return compose(
        ...GraphQLComponent,
        withApo
      )(WrapComponent)
    } catch (e) {
      return withApo(Component)
    }
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});

export { HOCQueryMutation }
