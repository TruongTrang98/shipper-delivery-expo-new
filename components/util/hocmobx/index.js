/* eslint-disable */
import { observer, inject } from 'mobx-react'

const HOCMobX = Component => {
  return inject(store => store.store)(observer(Component))
}

export { HOCMobX }
