import React from 'react'
import { View, Text } from 'react-native';
import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';

class DetailOrder extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      visible: false,
      dishes: null
    }
  }

  componentDidMount = () => {
    this.props.getRef(this)
  }

  render() {
    console.log(this.state.dishes);
    const dishesDetail = !!this.state.dishes
      ?
      <View>
        {
          this.state.dishes.map(item => {
          <Text>{item.name}</Text>
          })
        }
      </View>
      :
      ''
    return (
      <Dialog
        visible={this.state.visible}
        onTouchOutside={() => {
          this.setState({
            visible: false
          })
        }}
        footer={
          <DialogFooter>
            <DialogButton
              text="Đóng"
              onPress={() => { }}
            />
          </DialogFooter>
        }
      >
        <DialogContent>
          {dishesDetail}
        </DialogContent>
      </Dialog>
    )
  }
}

export default DetailOrder