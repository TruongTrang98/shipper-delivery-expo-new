import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux'
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    Image
} from 'react-native';
import bgImage from '../images/background.jpg';
import { Ionicons } from '@expo/vector-icons';

import { HOCQueryMutation, HOCMobX } from '../components/util'
import { LOGIN } from './_query'

const { width: WIDTH } = Dimensions.get('window');

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showPass: false,
            username: '',
            password: ''
        }
    }

    handleLogin = () => {
        const { username, password } = this.state
        const { Authen, mutate } = this.props
        if (!username || username.length === 0) {
            alert('Tên đăng nhập trống');
            return;
        }
        if (!password || password.length === 0) {
            alert('Mật khẩu trống');
            return;
        }
        console.log(username, password)
        mutate.login({
            variables: {
                input: {
                    username,
                    password
                }
            }
        }).then(res => {
            if (res.data.shipperLogin) {
                Authen.onLogin(res.data.shipperLogin.token)
                Actions.newOrderTab()
                console.log(res.data.shipperLogin.token)
                return
            } else {
                alert('Username hoặc Password không đúng')
            }
        }).catch(err => {
            alert(err)
        })
    }

    render() {
        return (
            <ImageBackground source={bgImage} style={styles.backgroundContainer}>
                <View>
                    {/* <Image source={{ uri: 'http://192.168.1.139:3000/images/logo.png' }}></Image> */}
                    <Text><Text style={{ fontSize: 70, color: '#139676', fontFamily: '' }}>S</Text><Text style={{ fontSize: 45, color: 'white' }}>FoodNow</Text></Text>
                </View>
                <View>
                    <Ionicons
                        name={'ios-person'}
                        size={28}
                        color={'white'}
                        style={styles.inputIcon}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder={'Tên đăng nhập'}
                        placeholderTextColor={'rgba(255,255,255, 0.7)'}
                        underlineColorAndroid="transparent"
                        onChangeText={text => this.setState({ username: text })}
                    />
                </View>
                <View>
                    <Ionicons
                        name={'ios-lock'}
                        size={28}
                        color={'white'}
                        style={styles.inputIcon}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder={'Mật khẩu'}
                        secureTextEntry={!this.state.showPass}
                        placeholderTextColor={'rgba(255,255,255, 0.7)'}
                        onChangeText={text => this.setState({ password: text })}
                    />

                    <TouchableOpacity
                        style={styles.btnEye}
                        onPress={() => this.setState(prevState => ({ ...prevState, showPass: !prevState.showPass }))}>
                        <Ionicons
                            name={this.state.showPass === true ? 'ios-eye' : 'ios-eye-off'}
                            style={styles.hiddenPass}
                            size={16}
                            color={'rgba(255,255,255, 0.7)'}
                        />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    style={styles.btnLogin}
                    onPress={this.handleLogin}>
                    <Text style={styles.text}>Đăng nhập</Text>
                </TouchableOpacity>
                <View style={styles.textCenter}>
                    <Text>Quên mật khẩu</Text>
                </View>
            </ImageBackground>
        );
    }
};

const styles = StyleSheet.create({
    backgroundContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    title: {
        fontSize: 50,
        color: 'red',
        marginBottom: 10,
    },
    input: {
        width: WIDTH - 70,
        height: 40,
        borderRadius: 25,
        fontSize: 16,
        paddingLeft: 45,
        margin: 10,
        backgroundColor: 'rgba(0,0,0, 0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginHorizontal: 25,
    },
    inputIcon: {
        position: 'absolute',
        top: 15,
        left: 40,
    },
    btnEye: {
        position: 'absolute',
        top: 19,
        right: 37,
    },
    btnLogin: {
        width: WIDTH - 140,
        height: 40,
        borderRadius: 25,
        justifyContent: 'center',
        marginTop: 10,
        backgroundColor: '#139676',
    },
    text: {
        color: 'rgba(255,255,255,0.7)',
        fontSize: 20,
        textAlign: 'center',
    },
    textCenter: {
        marginTop: 20,
        textAlign: 'center',
    },
    hiddenPass: {
        marginTop: 2
    }
});

export default HOCQueryMutation([
    {
        mutation: LOGIN,
        name: 'login'
    }
])(HOCMobX(Login));