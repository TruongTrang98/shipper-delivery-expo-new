import React, { Component } from 'react';
import { View, Text, Button, Alert, Platform } from 'react-native';
import { ListItem } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { Actions } from 'react-native-router-flux'

import { ORDER_SHIPPER_BOOKED, CHANGE_STATUS } from './_query'
import { HOCQueryMutation } from '../components/util';

import MapView from 'react-native-maps';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import Geocoder from 'react-native-geocoding';
// import Polyline from '@mapbox/polyline'

import MapViewDirections from 'react-native-maps-directions';

// import { withNavigationFocus } from 'react-navigation';

const GOOGLE_MAPS_APIKEY = 'AIzaSyAaqbCQHEd285IB_r5iBQdaNCxhwgGN1tY'
Geocoder.init(GOOGLE_MAPS_APIKEY);

class DonHangDangNhan extends Component {
  state = {
    location: null,
    shopLocation: null,
    userLocation: null,
    takeOrdered: false,
    orderBooked: null,
  }

  componentWillMount() {
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    // Location shipper
    this.setState({ location });
  };

  componentDidUpdate(prevProps) {
    let orderBooked = null;
    if (prevProps.isFocused !== this.props.isFocused) {
      this.props.client.query({
        query: ORDER_SHIPPER_BOOKED
      }).then(res => {
        orderBooked = res.data.orderShipperBooked
        this.setState({ orderBooked })
        if (!!orderBooked) {
          Geocoder.from(orderBooked.shop.address).then(res => {
            console.log("Finish Shop Address");
            this.setState({
              shopLocation: {
                latitude: res.results[0].geometry.location.lat,
                longitude: res.results[0].geometry.location.lng
              }
            })
          }).catch(err => console.log(err))
          Geocoder.from(orderBooked.address).then(res => {
            console.log("Finish User Address");
            this.setState({
              userLocation: {
                latitude: res.results[0].geometry.location.lat,
                longitude: res.results[0].geometry.location.lng
              }
            })
          }).catch(err => console.log(err))
        }
      })
    }
    //   if (!!this.state.orderBooked) {
    //     Geocoder.from(this.state.orderBooked.shop.address).then(res => {
    //       console.log("Finish Shop Address");
    //       this.setState({
    //         shopLocation: {
    //           latitude: res.results[0].geometry.location.lat,
    //           longitude: res.results[0].geometry.location.lng
    //         }
    //       })
    //     }).catch(err => console.log(err))
    //     Geocoder.from(this.state.orderBooked.address).then(res => {
    //       console.log("Finish User Address");
    //       this.setState({
    //         userLocation: {
    //           latitude: res.results[0].geometry.location.lat,
    //           longitude: res.results[0].geometry.location.lng
    //         }
    //       })
    //     }).catch(err => console.log(err))
    //   }
    // }
  }

  componentDidMount() {
    let { data: { orderShipperBooked } } = this.props
    if (!!orderShipperBooked) {
      Geocoder.from(orderShipperBooked.shop.address).then(res => {
        console.log("Finish Shop Address");
        this.setState({
          shopLocation: {
            latitude: res.results[0].geometry.location.lat,
            longitude: res.results[0].geometry.location.lng
          }
        })
      }).catch(err => console.log(err))
      Geocoder.from(orderShipperBooked.address).then(res => {
        console.log("Finish User Address");
        this.setState({
          userLocation: {
            latitude: res.results[0].geometry.location.lat,
            longitude: res.results[0].geometry.location.lng
          }
        })
      }).catch(err => console.log(err))
    }
    this.setState({
      orderBooked: orderShipperBooked,
    })
  }

  render() {
    // console.log(this.state.orderBooked);
    let { data: { orderShipperBooked, refetch }, mutate: { changeStatus } } = this.props
    const env = 'https://graphqldelivery.herokuapp.com/images'

    cancelOrder = async (orderId) => {
      try {
        const res = await changeStatus({
          variables: {
            _id: orderId, status: 'cancel'
          }
        })
        this.setState({
          orderBooked: null,
          takeOrdered: !this.state.takeOrdered,
          shopLocation: null,
          userLocation: null
        })
        if (res.data.changeStatus == '200') {
          Alert.alert(
            'Thông báo',
            'Hủy đơn hàng thành công!!!',
            [
              { text: 'OK', onPress: () => refetch },
            ],
            { cancelable: false },
          );
        }
      } catch (err) {
        console.log(err)
      }
    }

    shipperReceiveFood = async (orderId) => {
      try {
        const res = await changeStatus({
          variables: {
            _id: orderId, status: 'shipperReceiveFood'
          }
        })
        if (res.data.changeStatus == '200') {
          Alert.alert(
            'Thông báo',
            'Cảm ơn bạn đã nhận hàng từ cửa hàng, tiếp tục giao hàng cho khách hàng của mình nhé!!',
            [
              { text: 'OK', onPress: () => refetch },
            ],
            { cancelable: false },
          );
        }
      } catch (err) {
        console.log(err)
      }
    }

    finishOrder = async (orderId) => {
      try {
        const res = await changeStatus({
          variables: {
            _id: orderId, status: 'done'
          }
        })
        this.setState({
          orderBooked: null,
          takeOrdered: !this.state.takeOrdered,
          shopLocation: null,
          userLocation: null
        })
        if (res.data.changeStatus == '200') {
          Alert.alert(
            'Thông báo',
            'Chúc mừng bạn đã hoàn thành đơn hàng!!!',
            [
              { text: 'OK', onPress: () => refetch },
            ],
            { cancelable: false },
          );
        }
      } catch (err) {
        console.log(err)
      }
    }

    return (
      <>
        {
          !this.state.orderBooked
            ?
            <Text style={{ textAlign: 'center', fontSize: 20, color: 'red', marginTop: 10, marginBottom: 10 }}>Chưa nhận đơn hàng nào</Text>
            :
            <View onPress={() => console.log(1)}>
              <ListItem
                leftAvatar={{ source: { uri: `${env}/${this.state.orderBooked.shop.imageUrl}` } }}
                title={
                  this.state.orderBooked.shop.name.length < 35
                    ? this.state.orderBooked.shop.name
                    : Array.from(this.state.orderBooked.shop.name)
                      .splice(0, 28)
                      .join('') + '...'
                }
                subtitle={
                  <>
                    <View style={{ width: 200 }}>
                      <Text>
                        <Text style={{ fontWeight: 'bold', color: 'blue' }}>
                          From:{' '}
                        </Text>{' '}
                        {this.state.orderBooked.shop.address}
                      </Text>
                      <Ionicons name="ios-arrow-round-down" />
                      <Text>
                        <Text style={{ fontWeight: 'bold', color: 'red' }}>
                          To:{' '}
                        </Text>
                        {this.state.orderBooked.address}
                      </Text>
                      <Text>
                        <Text style={{ fontWeight: 'bold', color: 'red' }}>
                          ID đơn hàng:{' '}
                        </Text>
                        {this.state.orderBooked._id}
                      </Text>
                      <Text>
                        <Text style={{ fontWeight: 'bold', color: 'red' }}>
                          SĐT K/H:{' '}
                        </Text>
                        {this.state.orderBooked.phoneNumber}
                      </Text>
                    </View>
                    {/* <Text>----------------------------------------</Text> */}
                    {/* <View>
                      <Text style={{ fontSize: 16 }}>
                        <Ionicons name="ios-pricetag" /> Phí ship:
                      </Text>
                      <Text style={{ fontSize: 16 }}>
                        <Ionicons name="logo-usd" /> {''}
                        Tiền ứng trước:
                      </Text>
                      <Text style={{ fontSize: 16, color: 'red' }}>
                        <Ionicons name="ios-cash" /> Tổng:
                      </Text>
                    </View> */}
                    {/* <View style={{ position: 'absolute', right: 10, bottom: 0 }}>
                      <Text
                        style={{
                          position: 'relative',
                          left: 10,
                          fontSize: 16,
                        }}>
                        {this.state.orderBooked.costShip}đ
                      </Text>
                      <Text style={{ fontSize: 16 }}> {this.state.orderBooked.costDishes}đ</Text>
                      <Text style={{ fontSize: 16, color: 'red' }}>
                        {' '}
                        {+this.state.orderBooked.costShip + +this.state.orderBooked.costDishes}đ
                      </Text>
                      </View> */}
                    <View style={{ position: 'absolute', right: 7, top: 35 }}>
                      <Button
                        title="Chi tiết"
                        onPress={() => Actions.orderDetail({ item: this.state.orderBooked })}
                      />
                    </View>
                    <View style={{ position: 'absolute', right: 24, top: 85 }}>
                      <Button
                        title="Hủy"
                        style={{ color: 'red' }}
                        onPress={() =>
                          Alert.alert(
                            'Thông báo',
                            'Sau khi hủy đơn hàng, bạn phải gọi về trung tâm điều hành để trình bày lý do. Bạn có muốn hủy đơn hàng không?',
                            [
                              {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel',
                              },
                              {
                                text: 'OK',
                                onPress: () => cancelOrder(this.state.orderBooked._id)
                              },
                            ],
                            { cancelable: false },
                          )
                        }
                      />
                    </View>
                    <View style={{ position: 'absolute', top: 135, right: -10 }}>
                      <Button
                        title="Nhận hàng"
                        style={{ color: 'red' }}
                        onPress={() =>
                          Alert.alert(
                            'Thông báo',
                            'Bạn đã nhận đơn hàng phải không?',
                            [
                              {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel',
                              },
                              {
                                text: 'OK',
                                onPress: () => {
                                  this.setState({
                                    takeOrdered: !this.state.takeOrdered
                                  })
                                  shipperReceiveFood(this.state.orderBooked._id)
                                }
                              },
                            ],
                            { cancelable: false },
                          )
                        }
                      />
                    </View>
                    <View style={{ position: 'absolute', right: -2, top: 185 }}>
                      <Button
                        title="Hoàn tất"
                        onPress={() =>
                          Alert.alert(
                            'Thông báo',
                            'Bạn đã hoàn thành đơn hàng phải không?',
                            [
                              {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel',
                              },
                              {
                                text: 'OK',
                                onPress: () => {
                                  finishOrder(this.state.orderBooked._id)
                                },
                              },
                            ],
                            { cancelable: false },
                          )
                        }
                        style={{ backgroundColor: '#FF6347' }}
                      />
                    </View>
                  </>
                }
                bottomDivider
              />
            </View>
        }
        <View
          style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
          {/* {
            !!this.state.location && !!this.state.coords
              ?
              <MapView
                style={{ flex: 1 }}
                initialRegion={{
                  longitude: this.state.location.coords.longitude,
                  latitude: this.state.location.coords.latitude,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421
                }}
                showsUserLocation
              >
                {
                  !!this.state.coords
                  ?
                  <MapView.Polyline
                  strokeWidth={2}
                  strokeColor="red"
                  coordinates={this.state.coords}
                />
                :
                <Text>
                  Chưa tìm thấy vị trí
                </Text>
                }
              </MapView>
              :
              // <Text style={{ flex: 1, textAlign: 'center', fontSize: 20 }}>Vui lòng cấp quyền vị trí của bạn</Text>
              <Text style={{ flex: 1, textAlign: 'center', fontSize: 20 }}>Đang tìm đường đi cho bạn</Text>
          } */}
          {
            !!this.state.location
              ?
              <MapView
                style={{ flex: 1 }}
                initialRegion={{
                  longitude: this.state.location.coords.longitude,
                  latitude: this.state.location.coords.latitude,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421
                }}
                showsUserLocation
              >
                {
                  !this.state.takeOrdered && !!this.state.shopLocation
                    ?
                    <View>
                      <MapView.Marker coordinate={this.state.location.coords} />
                      <MapView.Marker coordinate={this.state.shopLocation} />
                      <MapViewDirections
                        origin={this.state.location.coords}
                        destination={this.state.shopLocation}
                        apikey={GOOGLE_MAPS_APIKEY}
                        strokeWidth={4}
                        strokeColor="#669DF6"
                      />
                    </View>
                    :
                    (
                      !!this.state.userLocation
                        ?
                        <View>
                          <MapView.Marker coordinate={this.state.location.coords} />
                          <MapView.Marker coordinate={this.state.userLocation} />
                          <MapViewDirections
                            origin={this.state.location.coords}
                            destination={this.state.userLocation}
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeWidth={4}
                            strokeColor="#669DF6"
                          />
                        </View>
                        :
                        null
                    )
                }
              </MapView>
              :
              <Text style={{ flex: 1, textAlign: 'center', fontSize: 20 }}>Vui lòng cấp quyền vị trí của bạn</Text>
            // <Text style={{ flex: 1, textAlign: 'center', fontSize: 20 }}>Đang tìm đường đi cho bạn</Text>
          }
        </View>
      </>
    );
  }
}

export default HOCQueryMutation([
  {
    query: ORDER_SHIPPER_BOOKED,
    name: 'orderShipperBooked'
  },
  {
    mutation: CHANGE_STATUS,
    name: 'changeStatus'
  }
])(DonHangDangNhan)
