import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native';

import { ListItem, withTheme, Divider } from 'react-native-elements'



class OrderDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showPass: false,
            username: '',
            password: ''
        }
    }

    render() {
        const { item, theme } = this.props

        const styles = {
            label: {
                color: theme.colors.primary,
            },
            value: {
                color: '#656665',
                width: '85%'
            },
            row: {
                height: 'auto',
                display: 'flex',
                flexDirection: 'row',
                marginLeft: 15,
                marginRight: 10,
            }
        }

        const dishes = item.dishes.map((dish, idx) => (
            <ListItem
                style={{ height: 'auto' }}
                key={idx}
                title={<Text style={styles.label}>{`${dish.name}  x  ${dish.count}`}</Text>}
                subtitle={
                    <>
                        <View style={{display: 'flex', flexDirection:'row'}}>
                            <Text style={styles.label}>Đơn giá: </Text>
                            <Text style={styles.value}>
                                {Intl.NumberFormat('vi-VN', {
                                    style: 'currency',
                                    currency: 'VND',
                                    maximumFractionDigits: 2
                                    }).format(dish.price)}
                            </Text>
                        </View>
                        <View style={{display: 'flex', flexDirection:'row'}}>
                            <Text style={styles.label}>Tổng giá: </Text>
                            <Text style={styles.value}>
                                {Intl.NumberFormat('vi-VN', {
                                    style: 'currency',
                                    currency: 'VND',
                                    maximumFractionDigits: 2
                                    }).format((+dish.price) * (+dish.count))}
                            </Text>
                            
                        </View>
                    </>
                }
                bottomDivider
                topDivider
            />
        ))

        return (
            <>
            <ScrollView>
                <View style={{flex: 1, backgroundColor: '#fff', minHeight: Dimensions.get('window').height}}>
                    <View style={{ height: 25 }} />
                    <View style={styles.row}>
                        <Text style={styles.label}>Tên khách hàng: </Text>
                        <Text style={styles.value}>{item.name}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.label}>Địa chỉ giao hàng: </Text>
                        <Text style={styles.value}>{item.address}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.label}>Số điện thoại: </Text>
                        <Text style={styles.value}>{item.phoneNumber}</Text>
                    </View>
                    <View style={{ height: 50 }} />
                    <View style={styles.row}>
                        <Text style={styles.label}>Cửa hàng: </Text>
                        <Text style={styles.value}>{item.shop.name}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.label}>Địa chỉ: </Text>
                        <Text style={styles.value}>{item.shop.address}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.label}>Số điện thoại: </Text>
                        <Text style={styles.value}>{item.shop.phoneNumber}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.label}>Email: </Text>
                        <Text style={styles.value}>{item.shop.email}</Text>
                    </View>
                    <View style={{ height: 50 }} />
                    <View style={styles.row}>
                        <Text style={{ ...styles.label, fontWeight: 'bold' }}>Danh sách món ăn: </Text>
                    </View>
                    <View style={{ height: 25 }} />
                    <View>
                        {dishes}
                    </View>
                    <View style={{ height: 25 }} />
                    <View style={styles.row}>
                        <Text style={styles.label}>Tổng tiền: </Text>
                        <Text style={styles.value}>
                            {Intl.NumberFormat('vi-VN', {
                                style: 'currency',
                                currency: 'VND',
                                maximumFractionDigits: 2
                                }).format(item.costDishes)}
                        </Text>
                    </View>
                    <View style={{ ...styles.row }}>
                        <Text style={styles.label}>Phí ship: </Text>
                        <Text style={styles.value}>
                            {Intl.NumberFormat('vi-VN', {
                                style: 'currency',
                                currency: 'VND',
                                maximumFractionDigits: 2
                                }).format(item.costShip)}
                        </Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.label}>Thành tiền: </Text>
                        <Text style={styles.value}>
                            {Intl.NumberFormat('vi-VN', {
                                style: 'currency',
                                currency: 'VND',
                                maximumFractionDigits: 2
                                }).format((+item.costShip) + (+item.costDishes))}
                        </Text>
                    </View>
                </View>
            </ScrollView>
            
        </>
        );
    }
};


export default withTheme(OrderDetail);