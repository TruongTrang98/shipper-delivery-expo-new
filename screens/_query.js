import gql from "graphql-tag";

export const LOGIN = gql`
    mutation($input: LoginInput!) {
        shipperLogin(input: $input) {
            token
        }
    }
`

export const CHANGE_STATUS = gql`
  mutation ($_id: String!, $status: String!) {
    changeStatus (_id: $_id, status: $status)
  }
`


export const INFO = gql`
    query {
        meShipper{
            _id
            name
            address
            imageUrl
            gender
            email
            phoneNumber
            cmnd
            isActive
            createdAt
            updatedAt
        }
    }
`

// Đơn hàng chưa shipper nào nhận
export const NEW_ORDER = gql`
    query{
        ordersShopReceived {
            _id
            shop {
                _id
                name
                address
                email
                phoneNumber
                imageUrl
            }
            name
            address
            phoneNumber
            costDishes
            costShip
            status
            createdAt
            updatedAt
            dishes{
                _id
                name
                count
                price
            }
        }
    }
`
// Shipper nhận đơn hàng mới
export const SHIPPER_BOOK_ORDER = gql`
    mutation($_id: String!){
        shipperBookOrder(_id: $_id)
    }
`

// Đơn hàng Shipper đang nhận
export const ORDER_SHIPPER_BOOKED = gql`
    query{
        orderShipperBooked {
            _id
            shop {
                _id
                name
                address
                phoneNumber
                email
                imageUrl
            }
            user {
                _id
                name
                imageUrl
                gender
                email
                phoneNumber
                address
            }
            dishes {
                _id
                name
                count
                price
            }
            address
            phoneNumber
            costDishes
            costShip
            status
            createdAt
            updatedAt
        }
    }
`

// Lịch sử đơn hàng Shipper đã giao
export const HISTORY_ORDERS = gql`
    query{
        shipperHistoryOrders{
            _id
            name
            address
            phoneNumber
            shop{
                _id
                name
                address
                phoneNumber
                email
                imageUrl
            }
            user {
                _id
                name
                imageUrl
                gender
                email
                phoneNumber
                address
            }
            dishes {
                _id
                name
                count
                price
            }
            address
            costDishes
            costShip
            status
            createdAt
            updatedAt
        }
    }
`

export const ORDERS_PUBLISH = gql`
  subscription {
    ordersShopPublish {
        _id
        shop {
            _id
            name
            address
            email
            phoneNumber
            imageUrl
        }
        name
        address
        phoneNumber
        costDishes
        costShip
        status
        createdAt
        updatedAt
        dishes{
            _id
            name
            count
            price
        }
    }
}
`