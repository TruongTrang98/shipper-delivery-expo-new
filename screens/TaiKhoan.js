/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux'
import { View, Text, Button, Dimensions } from 'react-native';
import { Avatar, Input } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import gql from "graphql-tag";

// import { INFO } from './_query'
import { HOCQueryMutation, HOCMobX } from '../components/util';

class TaiKhoan extends Component {
  constructor(props){
    super(props)
  }

  handleLogout = () => {
    this.props.Authen.onLogout()
    Actions.login()
  }

  render() {
    const { data: { meShipper, refetch } } = this.props
    const { _id, name, address, imageUrl, gender, email, phoneNumber, cmnd, createAt } = meShipper
    const env = 'https://graphqldelivery.herokuapp.com/images/'
    
    return (
      <>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ marginTop: 120, marginBottom: 30 }}>
            <Avatar
              size="xlarge"
              rounded
              source={{
                uri: `${env}/${imageUrl}`
              }}
            />
          </View>
          <View style={{ width: '90%', flex: 1 }}>
            <Input placeholder='Họ và tên'
              value={name}
              leftIcon={
                <Ionicons name='ios-person' size={22} style={{ marginRight: 10 }} />
              }
              disabled={false}
            />
            <Input placeholder='Email'
              value={email}
              leftIcon={
                <Ionicons name='ios-mail' size={22} style={{ marginRight: 10 }} />
              }
              disabled={false}
            />
            <Input placeholder='Địa chỉ'
              value={address}
              leftIcon={
                <Ionicons name='ios-home' size={22} style={{ marginRight: 10 }} />
              }
              disabled={false}
            />
            <Input placeholder='CMND'
              value={cmnd}
              leftIcon={
                <Ionicons name='ios-card' size={22} style={{ marginRight: 10 }} />
              }
              disabled={false}
            />
            <Input placeholder='Phone Number'
              value={phoneNumber}
              leftIcon={
                <Ionicons name='ios-call' size={22} style={{ marginRight: 10 }} />
              }
              disabled={false}
            />
          </View>
          <View style={{ marginTop:50, flex:1, flexDirection: 'row', width: '100%' }}>
            <View style={{ flex:1, marginLeft: 20, marginRight: 20 }}>
              <Button
                title="Đổi mật khẩu"
                onPress={() => Actions.changePassword()}
              />
            </View>
            <View style={{ flex:1, marginLeft: 20, marginRight: 20 }}>
              <Button
                title="Đăng xuất"
                onPress={() => this.handleLogout()}
                style={{ width: 50 }}
              />
            </View>
          </View>
        </View>
      </>
    );
  }
}
const INFO = gql`
    query {
        meShipper{
            _id
            name
            address
            imageUrl
            gender
            email
            phoneNumber
            cmnd
            isActive
            createdAt
            updatedAt
        }
    }
`

export default HOCQueryMutation([
  {
    query: INFO,
    name: 'meShipper'
  }
])(HOCMobX(TaiKhoan))
