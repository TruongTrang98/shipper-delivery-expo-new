import React, { useState } from 'react';
import { View, Text, ScrollView, Modal, TouchableHighlight, Dimensions, Alert, AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Ionicons } from '@expo/vector-icons';
import { Button, ListItem, Overlay } from 'react-native-elements';
import DetailOrder from '../components/modal/popupOrderDetail'

import { NEW_ORDER, SHIPPER_BOOK_ORDER, ORDERS_PUBLISH } from './_query'
import { HOCQueryMutation } from '../components/util';

let refModal = null
class DonHangMoi extends React.Component {
  constructor (props) {
    super(props)
  }

  acceptOrder = async (orderId) => {
    try {
      const res = await this.props.mutate.shipperBookOrder({
        variables: {
          _id: orderId
        }
      })
      await this.props.data.refetch()
      Alert.alert(
        'Thông báo',
        'Nhận đơn hàng thành công!!!',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      );
    } catch (error) {
      console.log(error)
    }
  }

  render () {
    const { data: { ordersShopReceived, refetch }, ordersShopPublish } = this.props
    const order = (!!ordersShopPublish.ordersShopPublish) ? ordersShopPublish.ordersShopPublish : ordersShopReceived
    return (
      <>  
        <ScrollView>        
          <>
            {order.map((order, index) => (
              <View key={index}>
                <ListItem
                  key={order._id}
                  leftAvatar={{ source: { uri: `https://graphqldelivery.herokuapp.com/images/${order.shop.imageUrl}` } }}
                  title={
                    order.shop.name.length < 35
                      ? order.shop.name
                      : Array.from(order.shop.name)
                        .splice(0, 28)
                        .join('') + '...'
                  }
                  subtitle={
                    <>
                      <View style={{ width: 200 }}>
                        <Text>
                          <Text style={{ fontWeight: 'bold', color: 'blue' }}>
                            From:{' '}
                          </Text>{' '}
                          {order.shop.address}
                        </Text>
                        <Ionicons name="ios-arrow-round-down" />
                        <Text>
                          <Text style={{ fontWeight: 'bold', color: 'red' }}>
                            To:{' '}
                          </Text>
                          {order.address}
                        </Text>
                        <Text>
                          -------------------------------
                          </Text>
                        <View>
                          <Text style={{ fontSize: 16 }}>
                            <Ionicons name="ios-pricetag" /> Phí ship:
                          </Text>
                          <Text style={{ fontSize: 16 }}>
                            <Ionicons name="logo-usd" /> {''}
                            Tiền đơn hàng:
                            </Text>
                          <Text style={{ fontSize: 16, color: 'red' }}>
                            <Ionicons name="ios-cash" /> Tổng:
                            </Text>
                        </View>
                        <View
                          style={{ position: 'absolute', right: -60, bottom: 0 }}>
                          <Text
                            style={{
                              position: 'relative',
                              left: 10,
                              fontSize: 16,
                            }}>
                            {order.costShip}đ
                            </Text>
                          <Text style={{ fontSize: 16 }}> {order.costDishes}đ</Text>
                          <Text style={{ fontSize: 16, color: 'red' }}>
                            {' '}
                            {+order.costShip + +order.costDishes}đ
                            </Text>
                        </View>
                      </View>
                      <View style={{ position: 'absolute', right: 0, top: 40 }}>
                        <Button
                          title="Chi tiết"
                          // onPress={() => refModal.setState({ visible: true, dishes: order.dishes })}
                          onPress={() => Actions.orderDetail({ item: order })}
                        />
                      </View>
                      <View style={{ position: 'absolute', right: 5, top: 90 }}>
                        <Button
                          title="Nhận"
                          color='red'
                          onPress={() => this.acceptOrder(order._id)}
                        />
                      </View>
                    </>
                  }
                  bottomDivider
                />
              </View>
            ))}
          </>
        </ScrollView>
      </>
    )
  }
  
}

export default HOCQueryMutation([
  {
    query: NEW_ORDER,
    name: 'newOrder'
  },
  {
    mutation: SHIPPER_BOOK_ORDER,
    name: 'shipperBookOrder'
  },
  {
    subcription: ORDERS_PUBLISH,
    name: 'ordersShopPublish'
  }
])(DonHangMoi)
