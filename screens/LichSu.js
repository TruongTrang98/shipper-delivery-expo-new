import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux'
import { View, Text, ScrollView, Button, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';

import { HISTORY_ORDERS } from './_query'
import { HOCQueryMutation } from '../components/util';
import { withNavigationFocus } from 'react-navigation';

class LichSu extends Component {
  state = {
    historyOrders: null
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      // console.log(this.props.client.query)
      this.props.client.query({
        query: HISTORY_ORDERS
      }).then(res => {
        const historyOrders = res.data.shipperHistoryOrders
        // delete orderBooked.__typename
        this.setState({ historyOrders })
      })
    }
  }


  componentDidMount() {
    const { data: { shipperHistoryOrders } } = this.props
    this.setState({
      historyOrders: shipperHistoryOrders
    })
  }

  render() {
    const env = 'https://graphqldelivery.herokuapp.com/images'
    return (
      <>
        {
          !this.state.historyOrders
            ?
            <Text style={{ textAlign: 'center', fontSize: 30, color: 'red', marginTop: 20 }}>Chưa giao đơn hàng nào</Text>
            :
            <ScrollView>
              <>
                {this.state.historyOrders.map((item, index) => (
                  <View key={index}>
                    <ListItem
                      leftAvatar={{ source: { uri: `${env}/${item.shop.imageUrl}` } }}
                      title={
                        item.shop.name.length < 35
                          ? item.shop.name
                          : Array.from(l.name)
                            .splice(0, 28)
                            .join('') + '...'
                      }
                      subtitle={
                        <>
                          <View style={{ width: 200 }}>
                            <Text>
                              <Text style={{ fontWeight: 'bold', color: 'blue' }}>
                                From:{' '}
                              </Text>{' '}
                              {item.shop.address}
                            </Text>
                            <Ionicons name="ios-arrow-round-down" />
                            <Text>
                              <Text style={{ fontWeight: 'bold', color: 'red' }}>
                                To:{' '}
                              </Text>
                              {item.address}
                            </Text>
                          </View>
                          <View style={{ position: 'absolute', right: 0 }}>
                            <Button
                              title="Chi tiết"
                              onPress={() => Actions.orderDetail({item: item})}
                            />
                          </View>
                        </>
                      }
                      bottomDivider
                    />
                  </View>
                ))}
              </>
            </ScrollView>
        }
      </>
    )
  }
}

export default withNavigationFocus(HOCQueryMutation([
  {
    query: HISTORY_ORDERS,
    name: 'shipperHistoryOrders'
  }
])(LichSu))
