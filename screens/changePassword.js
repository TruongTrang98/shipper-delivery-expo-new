/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux'
import { View, TextInput, Dimensions, StyleSheet, Alert } from 'react-native';
import { Input, Button } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import gql from "graphql-tag";

// import { INFO } from './_query'
import { HOCQueryMutation } from '../components/util';

const styles = StyleSheet.create({
    wrapper: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        backgroundColor: '#fff'
    },
    input: {
        width: '80%',
        marginTop: 20,
        marginRight: 'auto',
        marginLeft: 'auto',
        borderRadius: 10,
        borderBottomColor: '#000',
        borderBottomWidth: 1,
        height: '5%',
        fontSize: 20
    },
    button: {
        width: '50%',
        marginTop: 30,
        marginRight: 'auto',
        marginLeft: 'auto',
    }
})

class ChangePassword extends Component {
  constructor(props){
    super(props)
    this.state = {
      newPassword: '',
      oldPassword: '',
      confirmPassword: '',
      loading: false
    }
  }

  onChangePassword = () => {
    if(this.state.newPassword !== this.state.confirmPassword) {
      Alert.alert('Mật khẩu nhập lại không trùng khớp')
      this.setState({ oldPassword: '', newPassword: '', confirmPassword: '' })
    } else {
      this.setState({ loading: true })
      this.props.mutate.changePassword({
        variables: {
          input: {
            oldPassword: this.state.oldPassword,
            newPassword: this.state.newPassword
          }
        }
      }).then(res => {
        console.log(res)
        this.setState({ loading: false })
        if(res.data.changePassword === '200') {
          Alert.alert('Đổi mật khẩu thành công')
          this.setState({ oldPassword: '', newPassword: '', confirmPassword: '' })
        } else {
          Alert.alert('Đổi mật khẩu không đúng. Vui lòng nhập lại')
          this.setState({ oldPassword: '', newPassword: '', confirmPassword: '' })
        }
      }).catch(err => {
        this.setState({ loading: false })
        console.log(err)
      })
    }
  }
  

  render() {
    const hasError = !this.state.oldPassword || !this.state.newPassword || !this.state.confirmPassword
    return (
      <>
        <View style={styles.wrapper}>
            <TextInput 
              secureTextEntry={true} 
              style={styles.input}  
              placeholder='Nhập mật khẩu cũ' 
              containerStyle={styles.input} 
              onChangeText={(text) => this.setState({ oldPassword: text })} 
            />
            <TextInput 
              secureTextEntry={true} 
              style={styles.input}  
              placeholder='Nhập mật khẩu mới' 
              containerStyle={styles.input} 
              onChangeText={(text) => this.setState({ newPassword: text })} 
            />
            <TextInput 
              secureTextEntry={true} style={styles.input}  
              placeholder='Nhập lại mật khẩu mới' 
              containerStyle={styles.input} 
              onChangeText={(text) => this.setState({ confirmPassword: text })} 
            />
            <Button 
              title='Đổi mật khẩu' 
              buttonStyle={styles.button} 
              onPress={this.onChangePassword} 
              loading={this.state.loading} 
              disabled={hasError} 
            />
        </View>
      </>
    );
  }
}
const CHANGE_PASSWORD = gql`
    mutation($input: ChangePasswordInput!) {
        changePassword(input: $input)
    }
`

export default HOCQueryMutation([
  {
    mutation: CHANGE_PASSWORD,
    name: 'changePassword'
  }
])(ChangePassword)
